#!/usr/bin/env bash


file="./jq"
if [ -f "$file" ]
then
	echo "$file existe."
else
	echo "Download do arquivo $file ."
    wget -O jq https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64
	chmod +x ./jq
fi

CAMINHO=$(curl -v -X GET -H "Content-Type: application/json" http://192.168.56.133:2375/containers/pdci_jenkins/json | ./jq '.Mounts[0].Source' | sed 's/"//g')

echo "Caminho do volume do jenkins $CAMINHO/workspace/${JOB_BASE_NAME} ."

docker -H tcp://192.168.56.133:2375 run --network co7_gitlab_pdcinet  -v $CAMINHO/workspace/${JOB_BASE_NAME}:/root/src newtmitch/sonar-scanner sonar-scanner \
  -Dsonar.host.url=http://192.168.56.133:9000 \
  -Dsonar.projectKey=lafsisbio \
  -Dsonar.projectName="Laf-SisBio" \
  -Dsonar.projectVersion=1 \
  -Dsonar.projectBaseDir=/root/src \
  -Dsonar.verbose=true  \
  -Dsonar.login=efa8af74797c53c6deac9c37fa904e21c4d88a9a \
  -Dsonar.sources=./src \
  -Dsonar.sourceEncoding=UTF-8 \
  -Dsonar.scm.provider=git \
  -Dsonar.scm.enabled=true \
  -Dsonar.exclusions=vendor/**,node_modules/**,build/**,.svn/**,.git/**,jenkins,sql/**,.scannerwork,public/js/app.js,public/css/app.css

#Realisa os testes
docker -H tcp://192.168.56.133:2375 run --network co7_gitlab_pdcinet  -v $CAMINHO/workspace/${JOB_BASE_NAME}:/root/src newtmitch/sonar-scanner sonar-scanner \
  -Dsonar.host.url=http://192.168.56.133:9000 \
  -Dsonar.projectKey=lafsisbio \
  -Dsonar.projectName="Laf-SisBio" \
  -Dsonar.projectVersion=1 \
  -Dsonar.tests=./src/tests  \
  -Dsonar.projectBaseDir=/root/src \
  -Dsonar.verbose=true  \
  -Dsonar.login=efa8af74797c53c6deac9c37fa904e21c4d88a9a \
  -Dsonar.sources=./src/app \
  -Dsonar.sourceEncoding=UTF-8 \
  -Dsonar.php.coverage.reportPaths=phpunit.coverage.xml  \
  -Dsonar.php.tests.reportPath=phpunit.report.xml \
  -Dsonar.scm.provider=git \
  -Dsonar.scm.enabled=true \
  -Dsonar.exclusions=vendor/**,node_modules/**,build/**,.svn/**,.git/**,jenkins,sql/**,.scannerwork,public/js/app.js,public/css/app.css