#https://github.com/CentOS/CentOS-Dockerfiles
#https://github.com/CentOS/CentOS-Dockerfiles/tree/master/httpd/centos7
FROM registry.gitlab.com/pdci/co7_httpd_laravel
#MAINTAINER ICMBIO <roquebrasilia@gmail.com>
LABEL Vendor="COTEC-LAF-SISBio"
LABEL Description="Centos 7 , PHP 7.2, composer  "
LABEL License=GPLv2
LABEL Version=0.0.1

ENV APP_HOME /var/www/html

WORKDIR $APP_HOME

RUN rm -rf $APP_HOME/*

COPY conf/etc/httpd/conf/httpd.conf  /etc/httpd/conf/httpd.conf
COPY conf/laravel.conf /etc/httpd/conf.d/laravel.conf

COPY src/  $APP_HOME

#COPY conf/etc/php.ini /etc/php.ini
#COPY conf/etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf
COPY src/.env.example $APP_HOME/.env

RUN composer install
RUN yarn install
RUN php artisan key:generate

EXPOSE 443 80 8000 5001

RUN chown apache:apache $APP_HOME -R
RUN chmod 760 $APP_HOME -R

ADD run-httpd.sh /run-httpd.sh
RUN chmod -v +x /run-httpd.sh

CMD ["/run-httpd.sh"]