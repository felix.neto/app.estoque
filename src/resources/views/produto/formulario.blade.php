@extends('layouts.app')

@section('content')

@if (count($errors) > 0)
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif


<h1> Novo Produto </h1>


<form class="form-group" action="<?=action('ProdutoController@adiciona')?>" method="post">
    
    <label>Nome</label>
    <input class="form-control" name="nome" value="<?=old('nome')?>">


    <label>Descricao</label>
    <input class="form-control" name="descricao" value="<?= old('descricao') ?>" >


    <label>Valor</label>
    <input class="form-control" name="valor" value="<?=old('valor')?>">


    <label>Quantidade</label>
    <input type="number" class="form-control" name="quantidade" value="<?=old('quantidade')?>">
    
    <label>Tamanho</label>
    <input class="form-control" name="tamanho" value="<?=old('tamanho')?>">   
    
    <label>Categoria</label>
    <select class="form-control" name="categoria_id">       
        
        <?php foreach ( $categorias as $c  ){  ?> 
        
        <option value="  <?= $c->id ?>"  >   <?= $c->nome ?> </option>
        
        <?php } ?>

       
    </select>


    
    
    </br>
    
    <input type="hidden" name="_token" value="<?= csrf_token() ?>" />
    
  <button type="submit" class="btn btn-primary">Submit</button>
    
</form>

@stop  


