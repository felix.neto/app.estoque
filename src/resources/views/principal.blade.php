<html>
<head>
    
    <link rel="stylesheet" href="/estoque/public/css/app.css">
    <link rel="stylesheet" href="/estoque/public/css/custom.css">

    <link rel="stylesheet" href="/estoque/vendor/fontawesome/web-fonts-with-css/css/fontawesome-all.css">
    
    <!-- hotkeys: pp passa valor de php
                  aa abre tag php
                  ss fecha tag
    -->
    
    
    <title>Controle de estoque</title>
</head>

<body>
  <div class="container">

  <nav class="navbar navbar-default">
    <div class="container-fluid">

    <div class="navbar-header">   
        
        <a class="navbar-brand" href=" <?= url('/') ?> ">Estoque Laravel</a>
      
    </div>

      <ul class="nav navbar-nav navbar-right">
          
          <li><a href="<?= action('ProdutoController@lista') ?>">Listagem</a></li>
          <li><a href="<?= action('ProdutoController@novo') ?>">Novo</a></li>
      

        
      
      </ul>

    </div>
  </nav>
      

    @yield('conteudo')
    



    

    

  <footer class="footer">
      <p>© CGGP</p>
  </footer>

  </div>
</body>
</html>