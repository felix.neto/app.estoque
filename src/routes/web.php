<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 * 
 * 
 * http://localhost/estoque/public
*/



Route::get('/produtos', 'ProdutoController@lista');

Route::get('/produtos/mostra', 'ProdutoController@mostra');

Route::get('/produtos/novo', 'ProdutoController@novo');

Route::post('/produtos/adiciona', 'ProdutoController@adiciona');

Route::get('/produtos/remove', 'ProdutoController@remove');

Route::get('/produtos/edita', 'ProdutoController@edita');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'ProdutoController@lista');
