<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB; //esse eh o namespace da classe DB
use Request;
use App\Produto;
use App\Http\Requests\ProdutoRequest;
use App\Categoria;




class ProdutoController extends Controller {
    
    
        public function __construct()
    {
        $this->middleware('auth');
    }

    public function lista(){
        
       $produtos=Produto::all();
       
       
       //se eu der um dd($produtos) vai fazer um dump do q tem na array
       
      return view('produto/listagem')->with('produtos',$produtos);
      
      //return response()->json($produtos);
      
    }
    
    public function mostra(){
        
            $id=Request::input('id',0); //permite pegar os parametros da pagina, por exemplo o GET, o zero depois da virgula eh o default caso nao venha nada
                                        //            $input = Request::all();
                                        //            var_dump($input);
                                        //$url = Request::url();
                                        //O valor da $url seria http://localhost:8000/produtos/mostra, mas já no caso do path:
                                        //$uri = Request::path();
                                        //O valor da $uri seria produtos/mostra.
            
                 
       $resposta=Produto::find($id);
       
         if(empty($resposta)) {
            return "Esse produto não existe";
          }
          return view('produto/detalhes')->with('p', $resposta );

        }
    
        
        public function novo(){
            
            return view('produto/formulario')->with('categorias',Categoria::all());
            
        }
    
        public function adiciona(ProdutoRequest $request){
            
              $params=$request->all();
            
              
              $produto= new Produto($params);  

              $produto->save();
              
              
            return redirect()->action('ProdutoController@lista')->withInput(Request::only('nome'));
            
        }
        
            public function remove(){
            
            $id=Request::input('id',0);
                                     
            $produto=Produto::find($id);
            
            $produto->delete();
            
              
            return redirect()->action('ProdutoController@lista');
            
        }
        
            public function edita(){
                
                 $id=Request::input('id',0);
                 
                return view('produto/formulario')->with('id', $id );
            
        }
        

    
    
        //return redirect('/login");
        
     
 
        
    
    
    
    
    
    
}